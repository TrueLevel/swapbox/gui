# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import kivy
from kivy import Logger
from kivy.config import Config as KivyConfig

from src.app.app import TemplateApp
from src.config import Config, parse_args as parse_args

kivy.require("2.0.0")


def set_kivy_log_level(debug: bool) -> None:
    if debug:
        KivyConfig.set('kivy', 'log_level', 'debug')
    else:
        KivyConfig.set('kivy', 'log_level', 'warning')
    KivyConfig.write()


def set_window(fullscreen: bool) -> None:
    KivyConfig.set('graphics', 'top', 0)
    KivyConfig.set('graphics', 'left', 0)
    KivyConfig.set('graphics', 'resizable', 0)
    KivyConfig.set('graphics', 'allow_screensaver', 0)
    KivyConfig.set('graphics', 'verify_gl_main_thread', 1)

    if fullscreen:
        KivyConfig.set('graphics', 'fullscreen', 1)
        KivyConfig.set('graphics', 'show_cursor', 0)
        KivyConfig.set('graphics', 'borderless', 1)
    else:
        KivyConfig.set('graphics', 'width', 1280)
        KivyConfig.set('graphics', 'height', 720)
        KivyConfig.set('graphics', 'fullscreen', 0)
        KivyConfig.set('graphics', 'show_cursor', 1)
        KivyConfig.set('graphics', 'borderless', 0)


def main() -> None:
    args = parse_args()
    config = Config(args.config)

    set_window(config.is_fullscreen)
    set_kivy_log_level(config.debug)

    app = TemplateApp(config)
    try:
        app.run()
    except KeyboardInterrupt:
        Logger.info("exiting")


if __name__ == '__main__':
    main()
