# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
from time import sleep, time, time_ns
from typing import TypedDict

import argument  # type: ignore[import-untyped]
import zmq

port = '5558'

context = zmq.Context()
socket: zmq.Socket = context.socket(zmq.PUB)
socket.bind('tcp://*:{}'.format(port))

# change this to True if you want to emulate syncing issues
simulate_syncing = False

f = argument.Arguments()
# add a switch, a flag with no argument
f.switch("verbose",
         help="Verbose output",
         abbr="v"
         )
arguments, errors = f.parse()


def get_next_status(current_block: int) -> tuple[int, int, bool]:
    current_block += 1

    # for testing purposes, 3 out of 5 status messages are in sync
    return current_block, int(time()), current_block % 5 > 1 and simulate_syncing


blockType = TypedDict('blockType', {'number': int, 'timestamp': int})
blockchainType = TypedDict('blockchainType', {'current_block': blockType, 'syncing': bool})
systemType = TypedDict('systemType', {'temp': int, 'cpu': int})
msgType = TypedDict('msgType', {'blockchain': blockchainType, 'system': systemType})

msg : msgType = {
    'blockchain': {
        'current_block': {'number': 1000, 'timestamp': int(time())},
        'syncing': False,
    },
    'system': {
        'temp': 100,
        'cpu': 50,
    }
}

try:  # Command Interpreter
    while True:
        (msg['blockchain']['current_block']['number'],
         msg['blockchain']['current_block']['timestamp'],
         msg['blockchain']['syncing'],
         ) = get_next_status(msg['blockchain']['current_block']['number'])

        multipart = [b'status', json.dumps(msg).encode('utf-8')]

        if arguments["verbose"]:
            print(f" Sending {multipart}")
        socket.send_multipart(multipart)
        sleep(1)

except KeyboardInterrupt:  # If user do CTRL+C
    print("Exiting")
    exit(0)
