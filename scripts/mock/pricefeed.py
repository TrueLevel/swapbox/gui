# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json
import math
from time import sleep

import argument  # type: ignore[import-untyped]
import zmq
from zmq import Socket


class MockPricefeed:
    base_url = 'tcp://*'
    port_prices = 5556
    port_rep = 5559
    name = 'mock_pricefeed'

    increment = 0.025

    def __init__(self, verbose: bool):
        context = zmq.Context()

        # prepare sockets
        self.socket_prices: Socket = context.socket(zmq.PUB)
        self.socket_prices.bind(f'{self.base_url}:{self.port_prices}')

        # movement amplitude
        self.price_movement = 0.001

        # prices sinus start point
        self.price_i = "0.1"

        # prices update rate
        self.update_rate_s = 1.0

        # does it print message before sending them
        self.verbose = verbose

        # base prices, then oscillate around +- 0.1% of these prices
        self.base_prices = {
            "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2": {
                "token": "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                "symbol": "ETH",
                "buy_price": "4000.0",
                "buy_fee": "100",
                "sell_price": "3998.5",
                "sell_fee": "110"
            },
            "0x6b175474e89094c44da98b954eedeac495271d0f": {
                "token": "0x6b175474e89094c44da98b954eedeac495271d0f",
                "symbol": "DAI",
                "buy_price": "0.9",
                "buy_fee": "100",
                "sell_price": "0.896",
                "sell_fee": "110"
            },
            "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984": {
                "token": "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984",
                "symbol": "UNI",
                "buy_price": "16.13",
                "buy_fee": "100",
                "sell_price": "16.11",
                "sell_fee": "110"
            },
            "0x465e07d6028830124be2e4aa551fbe12805db0f5": {
                "token": "0x465e07d6028830124be2e4aa551fbe12805db0f5",
                "symbol": "XMR",
                "buy_price": "208.42",
                "buy_fee": "100",
                "sell_price": "205.3",
                "sell_fee": "110"
            },
        }

    # Compute next prices
    def next_prices(self) -> dict[str, dict[str, str]]:
        self.price_i = str(float(self.price_i) + self.increment)

        prices = self.base_prices.copy()
        for token, price in self.base_prices.items():
            prices[token]['buy_price'] = self.move_price(price['buy_price'], self.price_i)
            prices[token]['sell_price'] = self.move_price(price['sell_price'], self.price_i)
        return prices

    def move_price(self, base: str, i: str) -> str:
        return str(float(base) + self.price_movement * math.sin(float(i)) * float(base))

    def start(self) -> None:
        try:  # Command Interpreter
            while True:
                msg = {'prices': self.next_prices()}
                if self.verbose:
                    print(json.dumps(msg, indent=2))
                self.socket_prices.send_multipart([b"priceticker", json.dumps(msg).encode('utf-8')])
                sleep(self.update_rate_s)
        except KeyboardInterrupt:  # If user do CTRL+C
            print("Exiting")
            exit(0)


if __name__ == '__main__':
    f = argument.Arguments()
    f.switch("verbose", help="Verbose output", abbr="v")
    arguments, errors = f.parse()

    m = MockPricefeed(arguments["verbose"])
    m.start()
