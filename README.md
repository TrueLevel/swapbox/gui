[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

README
------

This is ALPHA software. Here be dragons!  Use at your own risk.

Contributions very! welcome!  0x64C9988A6C6EF250074D9A2d5df73a59d0292dd8


Development environment (Linux/mac)
-----------------------

#### Install zbar

```
# on mac:
brew install zbar

# on ubuntu:
apt-get install zbar-tools

# on linux:
apt-get install zbar
```

#### Installation

```
# Install python deps
pipenv install
# Install eSSP (until install with pipenv is fixed)
pipenv run pip3 install git+https://gitlab.com/truelevel/swapbox/eSSP
```

#### Running

```
pipenv run python main.py develop
```

### Mock backend

#### Simulating Note validator with develop config

```
pipenv run python scripts/mock/mock_validator.py
```

#### Simulating swap-box-web3 status with develop config

```
pipenv run python scripts/mock/mock_status.py [--verbose]
```

#### Simulating swap-box-web3 price feed with develop config

```
pipenv run python scripts/mock/mock_pricefeed.py [--verbose]
```

#### Simulating swap-box-web3 transactions with develop config

```
pipenv run python scripts/mock/mock_web3.py
```

#### OR just run all mock services

```
./scripts/mock/mock_all.sh
```

### Types

The code is fully type-annotated. To ensure your changes are annotated correctly, please run:

```shell
pipenv run mypy main.py src scripts
```

### Updating dependencies

If changing `Pipfile`, you should also run:

```shell
pipenv requirements > requirements.txt
```
OR
```shell
pipenv lock -r > requirements.txt
```
depending on your version of pipenv


RaspberryPi Setup Instructions
------------------------------

* RaspberryPi (4 B+ recommended)

```shell
pip install -r requirements.txt
```

```shell
sudo apt-get install devilspie2
```

### Running

```
DISPLAY=:0 KIVY_GL_BACKEND=sdl2 KIVY_WINDOW=sdl2 python main.py <config_file>
```

You can find configuration examples in the `config` folder.


### For setting splash screen

```shell
cp scripts/raspberry_pi4/splash.png /usr/share/plymouth/themes/pix/splash.png
```


### For inverted screen (PiCamera2)

```shell
cp --preserve=mode scripts/raspberry_pi4/screen_rotation/rotate /usr/local/bin/
```

```shell
cp scripts/raspberry_pi4/screen_rotation ~/.config/autostart/
```


### For inverted screen (Legacy PiCam w/ Opencv)

in /home/pi/.kivy/config.ini to invert touch x-axis:

    [input]
    mouse = mouse
    %(name)s = probesysfs,provider=hidinput,param=invert_x=1

in /boot/config.txt to set resolution for shitty screen (480x848)

    hdmi_group=2
    hdmi_mode=14

in /boot/config.txt to rotate screen 180 deg:

    display_rotate=2


Similar Projects
----------------

- [LightningATM](https://github.com/21isenough/LightningATM)

TO-DO
-----

- Get tx ID back from swap-box-web3 and show on final_buy_screen
- Admin interface??
- Multi fiat currency support ?

License
-------

[License: AGPL v3](/LICENSE.md)
Copyright &copy; TrueLevel SA


Contributors
------------

@tshabs
@samouraid3
@Minege
@megaturbo
@ymaktepi
@0xjac
@roflolilolmao
