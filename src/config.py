#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
from enum import Enum, auto
from typing import TypedDict

import strictyaml
from strictyaml import Bool, Float, Int, Map, Seq, Str


class CameraMethod(Enum):
    ZBARCAM = auto()
    OPENCV = auto()
    PICAMERA2 = auto()
    KIVY = auto()

    @staticmethod
    def elems() -> list[str]:
        return [elem.name for elem in CameraMethod]


class RelayMethod(Enum):
    PI_FACE = auto()
    GPIO = auto()
    NONE = auto()

    @staticmethod
    def elems() -> list[str]:
        return [elem.name for elem in RelayMethod]


MockType = TypedDict('MockType', {'enabled': bool, 'zmq_url': str})


class Mock:
    def __init__(self, cfg: MockType) -> None:
        self.enabled: bool = cfg["enabled"]
        self.zmq_url: str = cfg["zmq_url"]


NoteMachineType = TypedDict('NoteMachineType',
                            {'mock': MockType, 'port': str, 'recycler': bool, 'notes_currency': str})


class NoteMachine:
    def __init__(self, cfg: NoteMachineType) -> None:
        self.mock = Mock(cfg["mock"])
        self.port: str = cfg["port"]
        self.recycler: bool = cfg["recycler"]
        self.notes_currency: str = cfg["notes_currency"]
        # validate and parse note config file
        with open("{}/{}.yaml".format(Config._folder_notes_config, self.notes_currency)) as c:
            notes_config = strictyaml.load(c.read(), Config._notes_schema).data
        self.notes_values = notes_config["denominations"]


CameraType = TypedDict('CameraType', {'method': str, 'device': str})


class Camera:
    def __init__(self, cfg: CameraType) -> None:
        self.method: CameraMethod = CameraMethod[cfg["method"]]
        self.device: str = cfg["device"]


PricefeedType = TypedDict('PricefeedType', {'address': str, 'port_sub': int, 'port_req': int})


class Pricefeed:
    def __init__(self, cfg: PricefeedType) -> None:
        self.address: str = cfg["address"]
        self.port_sub: int = cfg["port_sub"]
        self.port_req: int = cfg["port_req"]


ZmqType = TypedDict('ZmqType', {'pricefeed': PricefeedType, 'rpc': str, 'status': str})


class Zmq:
    def __init__(self, cfg: ZmqType) -> None:
        self.machin: int = 2
        self.pricefeed: Pricefeed = Pricefeed(cfg["pricefeed"])
        self.rpc: str = cfg["rpc"]
        self.status: str = cfg["status"]


TokenType = TypedDict('TokenType', {'decimals': int, 'symbol': str, 'name': str, 'address': str})


class Token:
    def __init__(self, cfg: TokenType) -> None:
        self.decimals: int = cfg["decimals"]
        self.symbol: str = cfg["symbol"]
        self.name: str = cfg["name"]
        self.address: str = cfg["address"]

    def __repr__(self) -> str:
        return f"Token({self.name}, {self.symbol}, {self.address})"


BackendType = TypedDict('BackendType',
                        {'type': str, 'address': str, 'ticker': str, 'tokens': list[TokenType]})


class Backend:
    def __init__(self, cfg: BackendType) -> None:
        self.type: str = cfg["type"]
        self.address: str = cfg["address"]
        self.ticker: str = cfg["ticker"]
        self.tokens: list[Token] = [Token(t) for t in cfg["tokens"]]


class Config(object):
    """Represents Swapbox GUI config"""

    # config schema for strictyaml validation
    _schema = Map({
        "name": Str(),
        "debug": Bool(),
        "default_lang": Str(),
        "fiat_currency": Str(),
        "backends": Seq(Map({
            "type": Str(),
            "address": Str(),
            "ticker": Str(),
            "tokens": Seq(Map({
                "decimals": Int(),
                "symbol": Str(),
                "name": Str(),
                "address": Str()
            }))
        })),
        "note_machine": Map({
            "mock": Map({
                "enabled": Bool(),
                "zmq_url": Str(),
            }),
            "port": Str(),
            "recycler": Bool(),
            "notes_currency": Str()
        }),
        "camera": Map({
            "method": strictyaml.Enum(CameraMethod.elems()),
            "device": Str()
        }),
        "zmq": Map({
            "pricefeed": Map({
                "address": Str(),
                "port_sub": Int(),
                "port_req": Int()
            }),
            "rpc": Str(),
            "status": Str(),
        }),
        "relay_method": strictyaml.Enum(RelayMethod.elems()),
        "admin_pin": Int(),
        "is_fullscreen": Bool(),
        "default_slippage": Float(),
        "buy_limit": Int()
    })
    _notes_schema = Map({"denominations": Seq(Int())})

    _folder_config = "config"
    _folder_notes_config = "{}/{}".format(_folder_config, "notes")

    def __init__(self, config_name: str) -> None:
        # validate and parse config file
        with open("{}/{}.yaml".format(Config._folder_config, config_name)) as c:
            machine_config = strictyaml.load(c.read(), Config._schema).data

        # build self with parsed config
        self.operator_name = machine_config["name"]
        self.debug: bool = machine_config["debug"]
        self.default_lang: str = machine_config["default_lang"]
        self.fiat_currency: str = machine_config["fiat_currency"]
        self.backends = [Backend(b) for b in machine_config["backends"]]
        self.note_machine = NoteMachine(machine_config["note_machine"])
        self.camera = Camera(machine_config["camera"])
        self.zmq = Zmq(machine_config["zmq"])
        self.relay_method: RelayMethod = RelayMethod[machine_config["relay_method"]]
        self.is_fullscreen: bool = machine_config["is_fullscreen"]
        self.default_slippage: float = machine_config["default_slippage"]
        self.buy_limit: int = machine_config["buy_limit"]

        if not os.uname()[4].startswith("arm"):
            self.RELAY_METHOD = RelayMethod.NONE


def parse_args() -> argparse.Namespace:
    """
    Parse CLI arguments.

    Raises an argparse.ArgumentError if the config is missing.

    :return: Config attributes in an argparse Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config',
        help="configuration file name (located in machine_config/ folder)"
    )
    return parser.parse_args()
