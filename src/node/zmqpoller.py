import contextlib
import json
import time
import typing
from functools import partial
from threading import Event, Thread
from typing import Any, Generator, Optional

import zmq
from kivy import Logger
from kivy.clock import Clock
from pydantic import ValidationError

from src.node.pricefeed_subscriber import PricefeedResponse
from src.node.status_subscriber import Status

if typing.TYPE_CHECKING:
    from src.app.app import TemplateApp

_ns_per_second = 1e9


class ZMQPoller(Thread):
    TOPIC_STATUS = b"status"
    TOPIC_PRICES = b"priceticker"
    RAW_LEN_STATUS = RAW_LEN_PRICES = 2
    STATUS_RAW_TOPIC_IDX = PRICES_RAW_TOPIC_IDX = 0
    STATUS_RAW_MSG_IDX = PRICES_RAW_MSG_IDX = 1

    def __init__(self,
                 addr_status: str,
                 addr_prices: str,
                 app: "TemplateApp",
                 *args: Any,
                 ctx: Optional[zmq.Context] = None,
                 **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        self.addr_status = addr_status
        self.addr_prices = addr_prices
        self.app = app
        self.ctx = ctx if ctx is not None else zmq.Context.instance()

        self.syncing = True
        self.syncing_update_evt = Clock.schedule_once(self.app.show_sync_popup)
        self.prices_update_evt = Clock.schedule_once(partial(self.app.update_prices, {}))
        self.prices_update_threshold = 10 * _ns_per_second  # TODO Expose this in config?
        # Subtraction to ensure first real update is considered.
        self._last_prices_update_ts = time.monotonic_ns() - self.prices_update_threshold - 1

        self.update_prices = Event()

        self.running = Event()

    def run(self) -> None:
        self.running.set()
        with self._create_sockets() as (status_sock, prices_sock):
            poller = zmq.Poller()
            poller.register(status_sock)
            poller.register(prices_sock)

            while self.running.is_set():
                evts = dict(poller.poll())

                for sock, evt_mask in evts.items():
                    if evt_mask != zmq.POLLIN:  # Should not happen.
                        Logger.error(f"{self.name}: skip, unsupported sock evt {evts[status_sock]}")
                        continue

                    if status_sock == sock:
                        status_data = status_sock.recv_multipart(copy=True)
                        self._handle_status_msg(status_data)
                    elif prices_sock == sock:
                        prices_data = prices_sock.recv_multipart(copy=True)
                        self._handle_prices_msg(prices_data)
                    else:  # Should not happen.
                        Logger.error(f"{self.name}: skip, unknown socket")
                        continue

        Logger.debug(f"{self.name}: terminated")

    def stop(self) -> None:
        self.running.clear()

    def _handle_status_msg(self, raw: list[bytes]) -> None:
        if (l := len(raw)) != self.RAW_LEN_STATUS:
            Logger.warn(f"{self.name}: skip, bad status msg length {l}, want {self.RAW_LEN_STATUS}")
            return

        topic, msg = raw[self.STATUS_RAW_TOPIC_IDX], raw[self.STATUS_RAW_MSG_IDX]
        if topic != self.TOPIC_STATUS:
            Logger.warn(
                f"{self.name}: skip, bad status topic {topic!r}, want {self.TOPIC_STATUS!r}")
            return

        try:
            status = Status(**json.loads(msg))
        except json.JSONDecodeError as err:
            Logger.error(f"{self.name}: failed to decode status msg: {err}")
        except ValidationError as err:
            Logger.error(f"{self.name}: invalid status msg: {err}")
        else:
            if self.syncing == status.blockchain.syncing:
                return

            self.syncing = status.blockchain.syncing
            self.syncing_update_evt.cancel()  # Avoid needless mainthread calls.
            self.syncing_update_evt = Clock.schedule_once(self.app.show_sync_popup if self.syncing
                                                          else self.app.hide_sync_popup)

    def _handle_prices_msg(self, raw: list[bytes]) -> None:
        # If prices updates are not needed and the last update is younger than the given threshold,
        # don't update the prices.
        # The threshold, ensures price data is present fresh-ish when first displayed.
        if (not self.update_prices.is_set() and
                time.monotonic_ns() - self._last_prices_update_ts < self.prices_update_threshold):
            return

        if (l := len(raw)) != self.RAW_LEN_PRICES:
            Logger.warn(f"{self.name}: skip, bad prices msg length {l}, want {self.RAW_LEN_PRICES}")
            return

        topic, msg = raw[self.PRICES_RAW_TOPIC_IDX], raw[self.PRICES_RAW_MSG_IDX]
        if topic != self.TOPIC_PRICES:
            Logger.warn(
                f"{self.name}: skip, bad prices topic {topic!r}, want {self.TOPIC_PRICES!r}")
            return

        try:
            resp = PricefeedResponse(**json.loads(msg))
        except json.JSONDecodeError as err:
            Logger.error(f"{self.name}: failed to decode prices msg: {err}")
        except ValidationError as err:
            Logger.error(f"{self.name}: invalid prices msg: {err}")
        else:
            self.prices_update_evt.cancel()  # Avoid needless mainthread calls.
            self.prices_update_evt = Clock.schedule_once(
                partial(self.app.update_prices, resp.prices))
            self._last_prices_update_ts = time.monotonic_ns()

    @contextlib.contextmanager
    def _create_sockets(self) -> Generator[tuple[zmq.Socket, zmq.Socket], None, None]:
        status_sock: zmq.Socket
        prices_sock: zmq.Socket

        with self.ctx:
            try:
                status_sock = self.ctx.socket(zmq.SUB)
                status_sock.connect(self.addr_status)
                status_sock.subscribe(self.TOPIC_STATUS)

                prices_sock = self.ctx.socket(zmq.SUB)
                prices_sock.connect(self.addr_prices)
                prices_sock.subscribe(self.TOPIC_PRICES)

                yield status_sock, prices_sock

            finally:
                if status_sock is not None:
                    status_sock.close()

                if prices_sock is not None:
                    prices_sock.close()
