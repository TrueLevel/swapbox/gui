# Swap-box
# Copyright (C) 2022  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod


class LedDriver(ABC):
    """
    LED drivers base class, controls LED's lighting

    `led_on()` must turn on the LEDs, `led_off()` must turn off the LEDs and
    `close()` will be called at the end of the process if you need some end
    logic.
    """

    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def led_on(self) -> None:
        pass

    @abstractmethod
    def led_off(self) -> None:
        pass

    @abstractmethod
    def close(self) -> None:
        pass
