#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from src.config import RelayMethod
from src.led.led_driver_base import LedDriver


def load_led_driver(relay_method: RelayMethod) -> LedDriver:
    """
    Dynamically load the LED driver corresponding to given relay method.

    LED drivers packages are imported at runtime.
    """
    if relay_method is RelayMethod.PI_FACE:
        from src.led.piface_led_driver import LedDriverPiFace
        return LedDriverPiFace()
    elif relay_method is RelayMethod.GPIO:
        from src.led.gpio_led_driver import LedDriverGPIO
        return LedDriverGPIO()
    else:
        from src.led.no_led_driver import LedDriverNone
        return LedDriverNone()
