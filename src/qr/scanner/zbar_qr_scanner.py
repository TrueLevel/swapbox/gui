#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional

from src.qr.scanner.qr_scanner_cmd import QrScannerCmd


class QrScannerZbar(QrScannerCmd):
    """QR Scanner using Zbar

    >>>
    """

    def __init__(self, video_port: object):
        cmd = 'zbarcam --prescale=96x72 {}'.format(video_port)
        super().__init__(cmd)

    @property
    def sync(self) -> bool:
        return True

    def _is_qr_found(self, line: Optional[bytes]) -> bool:
        if line:
            return line.startswith(b"QR-Code:")
        return False

    def _get_qr_from_line(self, line: bytes) -> Optional[bytes]:
        return line[8:]

    def _start_locally(self) -> None:
        pass

    def _stop_locally(self) -> None:
        pass

    def _hide_overlay(self) -> None:
        pass

    def _show_overlay(self) -> None:
        pass

    def hide_overlay(self) -> None:
        pass

    def show_overlay(self) -> None:
        pass
