# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import abstractmethod
from typing import Optional


class QrScanner():

    def __init__(self) -> None:
        """ cmd: the command to execute to start a process that returns a QRcode on the std output"""
        super().__init__()

    @abstractmethod
    def _is_qr_found(self, line: Optional[bytes]) -> bool:
        """ parse the line to check if a QR code was found
        the line is the output of a process running the _cmd command"""
        pass

    @abstractmethod
    def _get_qr_from_line(self, line:bytes) -> Optional[bytes]:
        """ parse the line to return the content of the QR code,
        the line has already passed the check in _is_qr_found
        the line is the output of a process running the _cmd command"""
        pass

    @abstractmethod
    def _start_locally(self) -> None:
        """ qr_scanning is going to be started """
        pass

    @abstractmethod
    def _stop_locally(self) -> None:
        """ qr_scanning is going to be stopped, close stuff gracefully """
        pass

    @abstractmethod
    def hide_overlay(self) -> None:
        """
        Hide overlay

        No need to do anything if there's no overlay in the driver implementation.
        """
        pass

    @abstractmethod
    def show_overlay(self) -> None:
        """
        Show overlay

        No need to do anything if there's no overlay in the driver implementation.
        """
        pass
