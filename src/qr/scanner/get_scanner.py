#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from src.config import Camera, CameraMethod
from src.qr.scanner.qr_scanner_base import QrScanner


def get_scanner(camera: Camera) -> QrScanner:
    if camera.method is CameraMethod.ZBARCAM:
        from src.qr.scanner.zbar_qr_scanner import QrScannerZbar
        return QrScannerZbar(camera.device)
    elif camera.method is CameraMethod.OPENCV:
        from src.qr.scanner.opencv_qr_scanner import QrScannerOpenCV
        return QrScannerOpenCV()
    elif camera.method is CameraMethod.PICAMERA2:
        from src.qr.scanner.picamera_qr_scanner import QrScannerPiCamera2
        return QrScannerPiCamera2()
    else:
        from src.qr.scanner.none_qr_scanner import QrScannerNone
        return QrScannerNone()
