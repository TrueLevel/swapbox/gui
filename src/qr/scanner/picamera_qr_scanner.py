#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from src.qr.scanner.qr_scanner_base import QrScanner
from typing import Any, Optional, Callable
from functools import partial
from kivy.clock import Clock, mainthread


class QrScannerPiCamera2(QrScanner):

    def __init__(self) -> None:
        
        self._callback: Callable = lambda arg: arg

        super().__init__()

    @property
    def sync(self) -> bool:
        return False

    def scan(self, qr_reader: Any, callback: Callable) -> None:
        """ scans the interface for a QR-Code
        returns None if no code was found, or a string representing the whole QR """

        self._callback = callback
        self._qr_reader = qr_reader

        self._qr_reader.qr_code = None
        self._qr_reader.text = ""
        self._qr_reader.qr_code_delay = True

        qr_reader.bind(on_scan_result=self._is_qr_found)
        qr_reader.connect_camera(enable_analyze_pixels = True,
                                         enable_video = False)
        Clock.schedule_once(partial(self.camera_delay_off, qr_reader), 1)

    @mainthread  # type: ignore[misc]
    def camera_delay_off(self, qr_reader: Any, dt: int) -> None:
        self._qr_reader.qr_code_delay = False
        self._qr_reader.annotations = []

    @mainthread  # type: ignore[misc]
    def _is_qr_found(self, line: Optional[bytes]) -> None:
        """ parse the line to check if a QR code was found
        the line is the output of a process running the _cmd command"""
        self._callback(self._qr_reader.qr_code)

    def stop_scan(self) -> None:
        self._qr_reader.disconnect_camera()
        self._qr_reader.unbind(on_scan_result=self._is_qr_found)

    def _get_qr_from_line(self, line: bytes) -> Optional[bytes]:
        pass

    def _start_locally(self) -> None:
        pass

    def _stop_locally(self) -> None:
        pass

    def _hide_overlay(self) -> None:
        pass

    def _show_overlay(self) -> None:
        pass

    def hide_overlay(self) -> None:
        pass

    def show_overlay(self) -> None:
        pass
