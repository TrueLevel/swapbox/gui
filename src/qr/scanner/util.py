# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
from enum import Enum, auto

from eth_utils import to_checksum_address, is_address, is_checksum_address

address_without_checksum = (re.compile('^[0-9a-f]{40}$'), re.compile('^[0-9A-F]{40}$'))


class Network(Enum):
    ethereum = auto()
    zksync = auto()

    def validate_address(self, addr: str) -> str:

        raw_addr = addr

        if self not in (self.ethereum, self.zksync):
            raise ValueError('unsupported network')

        if (addr.startswith('0x') or addr.startswith('0X')):
            addr = addr[2:]

        addr_len = len(addr)
        if addr_len < 40:
            raise ValueError('address is too short')
        elif addr_len > 40:
            raise ValueError('address is too long')

        if not is_address(raw_addr):
            raise ValueError('invalid address')

        if not any(bool(pattern.match(addr)) for pattern in address_without_checksum):
            if not is_checksum_address(raw_addr):
                raise ValueError('invalid checksum')

        return f"{raw_addr}"


def parse_address(raw_addr: str, _: bool = False) -> tuple[str, str]:
    if not raw_addr:
        raise ValueError('invalid address')

    network, addr = raw_addr.split(':', 1) if ':' in raw_addr else ('ethereum', raw_addr)
    network = network.lower()

    if not addr:
        raise ValueError('invalid address')

    try:
        addr = Network[network].validate_address(addr)
    except KeyError:
        raise ValueError("invalid network: '{}'".format(network))

    return network, addr
