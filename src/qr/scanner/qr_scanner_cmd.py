# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import threading
from abc import abstractmethod
from typing import Any, Optional

import pexpect
from kivy import Logger
from src.qr.scanner.qr_scanner_base import QrScanner


class QrScannerCmd(QrScanner):

    def __init__(self, cmd: Any) -> None:
        """cmd: Command to execute to start a process, which returns a QRcode value on stdout."""
        super().__init__()
        self.scanning = threading.Event()
        self._cmd_to_execute = cmd
        self._execute: Optional[pexpect.spawn] = None

    def scan(self) -> str:
        """Start the cmd to scan for a QR-Code.

        :return None if no code was found, otherwise a string representing the QR code value.
        """

        self._start_locally()
        self.scanning.clear()
        self._execute = pexpect.spawn(self._cmd_to_execute, [], 300)

        while not self.scanning.is_set():
            try:
                self._execute.expect('\n')
                # Get last line from expect
                line = self._execute.before
                if self._is_qr_found(line):
                    qr = self._get_qr_from_line(line)
                    self._close_executor()
                    return qr.decode('ascii').strip() if qr is not None else ''
            except pexpect.EOF:
                break
            except pexpect.TIMEOUT:
                break
            except OSError as err:
                Logger.error(f"QR-SCAN: Unexpected error while scanning: {err}")
                break

        self._close_executor()
        return ''

    def _close_executor(self) -> None:
        self._stop_locally()
        if self._execute is not None:
            try:
                self._execute.close(True)
            except Exception as err:
                Logger.error(f"QR-SCAN: Failed to close executor: {err}")
            self._execute = None

    def stop_scan(self) -> None:
        self.scanning.set()
        self._close_executor()

    @abstractmethod
    def _is_qr_found(self, line: Optional[bytes]) -> bool:
        """ parse the line to check if a QR code was found
        the line is the output of a process running the _cmd command"""
        pass

    @abstractmethod
    def _get_qr_from_line(self, line: bytes) -> Optional[bytes]:
        """ parse the line to return the content of the QR code,
        the line has already passed the check in _is_qr_found
        the line is the output of a process running the _cmd command"""
        pass

    @abstractmethod
    def _start_locally(self) -> None:
        """ qr_scanning is going to be started """
        pass

    @abstractmethod
    def _stop_locally(self) -> None:
        """ qr_scanning is going to be stopped, close stuff gracefully """
        pass

    @abstractmethod
    def hide_overlay(self) -> None:
        """
        Hide overlay

        No need to do anything if there's no overlay in the driver implementation.
        """
        pass

    @abstractmethod
    def show_overlay(self) -> None:
        """
        Show overlay

        No need to do anything if there's no overlay in the driver implementation.
        """
        pass
