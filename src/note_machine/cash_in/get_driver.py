#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from src.config import NoteMachine
from src.config import NoteMachine
from src.note_machine.cash_in.cashin_driver_base import CashinCallback, CashinDriver


def get_driver(note_machine: NoteMachine, callback: CashinCallback) -> CashinDriver:
    if note_machine.mock.enabled:
        from .mock_cashin_driver import MockCashinDriver
        return MockCashinDriver(callback, note_machine.mock.zmq_url)
    else:
        from .essp_cashin_driver import EsspCashinDriver
        return EsspCashinDriver(callback, note_machine.port, note_machine.recycler, note_machine.notes_values)
