#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from time import sleep
from eSSP import EsspController
from eSSP.status import Status
from src.note_machine.cash_in.cashin_driver_base import CashinCallback, CashinDriver

i = 0


class EsspCashinDriver(CashinDriver):

    def __init__(self, callback_message: CashinCallback, validator_port: str, has_recycler: bool, notes: list) -> None:
        super().__init__(callback_message)
        self._validator_port = validator_port
        self._validator_has_recycler = has_recycler
        self.map_channel_notes = dict((n[0], n[1]) for n in enumerate(notes, 1))

    def _start_cashin(self) -> None:
        #  Create a new object ( Validator Object ) and initialize it
        print("Start cashin")
        validator = EsspController(com_port=self._validator_port, ssp_address="0", nv11=False, debug=True)
        self._supported_currencies = validator.supported_currencies
        print(self._supported_currencies)

        global i

        if self._validator_has_recycler:
            EsspCashinDriver._setup_validator(validator, dict(self.map_channel_notes.values()))

        while not self._stop_cashin.is_set():
            last_event = validator.poll_next_event()

            if last_event is None:
                continue

            (note, currency, event) = last_event

            if event == Status.SSP_POLL_CREDIT:
                if note not in self.map_channel_notes.keys():
                    continue
                value = self.map_channel_notes[note]
                self._callback_message("CHF:{}".format(value))

        EsspCashinDriver._close_validator(validator)

    @staticmethod
    def _setup_validator(validator: EsspController, notes: dict) -> None:
        # all notes are sent to cashbox
        for note in notes:
            validator.set_route_cashbox(int(note))
            sleep(0.5)

    @staticmethod
    def _close_validator(validator: EsspController) -> None:
        validator.disable_validator()
        sleep(1)
        validator.close()
        validator.stop()
