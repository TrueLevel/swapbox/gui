#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
from typing import Optional

from eSSP import EsspController
from src.note_machine.cash_out.cashout_driver_base import CashoutDriver


class EsspCashoutDriver(CashoutDriver):
    _MAP_CHANNEL_NOTES = {
        1: 10,
        2: 20,
        3: 50,
        4: 100,
        5: 200,
    }

    def __init__(self, validator_port: str, has_recycler: bool, notes: list) -> None:
        self._validator_port = validator_port
        self.validator: Optional[EsspController] = None
        self._validator_has_recycler = has_recycler
        self.map_channel_notes = dict((n[0], n[1]) for n in enumerate(notes, 1))

    def start_cashout(self) -> None:
        #  Create a new object ( Validator Object ) and initialize it
        self.validator = EsspController(com_port=self._validator_port, ssp_address="0", nv11=False, debug=True)

    def stop_cashout(self) -> None:
        self._close_validator()

    def get_balance(self) -> dict[int, int]:
        balance: dict[int, int] = {}
        if self.validator is None:
            return balance

        for note in self.map_channel_notes.values():
            balance[note] = self.validator.get_note_amount(int(note))
            time.sleep(0.5)

        return balance

    def do_cashout(self, amount: int, currency: str) -> None:
        if self.validator is None:
            return
        self.validator.payout(int(amount), currency)

    def _close_validator(self) -> None:
        if self.validator is None:
            return
        self.validator.close()
