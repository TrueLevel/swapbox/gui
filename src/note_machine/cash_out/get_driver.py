#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from src.config import NoteMachine
from src.note_machine.cash_out.cashout_driver_base import CashoutDriver


def get_driver(note_machine: NoteMachine) -> CashoutDriver:
    if note_machine.mock.enabled:
        from src.note_machine.cash_out.mock_cashout_driver import MockCashoutDriver
        return MockCashoutDriver()
    else:
        from src.note_machine.cash_out.essp_cashout_driver import EsspCashoutDriver
        return EsspCashoutDriver(note_machine.port, note_machine.recycler, note_machine.notes_values)
