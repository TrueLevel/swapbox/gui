# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod
import itertools
from kivy import Logger

class CashoutDriver(ABC):
    _MAP_CHANNEL_NOTES = {
        1: 10,
        2: 20,
        3: 50,
        4: 100,
        5: 200,
    }

    @abstractmethod
    def start_cashout(self) -> None:
        pass

    @abstractmethod
    def stop_cashout(self) -> None:
        pass

    @abstractmethod
    def get_balance(self) -> dict[int, int]:
        pass

    @abstractmethod
    def do_cashout(self, amount: int, currency: str) -> None:
        pass

    @staticmethod
    def check_available_notes(balance: dict[int, int], amount: int) -> bool:

        Logger.debug("Cashout amount: {}".format(amount))

        notes = CashoutDriver._MAP_CHANNEL_NOTES.values()
        note_counter = []

        for note, distribute in zip(notes, balance.values()):
            for n in range(distribute):
                note_counter.append(note)

        result = [seq for i in range(len(note_counter), 0, -1)
                  for seq in itertools.combinations(note_counter, i)
                  if sum(seq) == amount]
        Logger.debug("Possible note combinations: {}".format(result))

        if result:
            return True
        else:
            return False
