#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
from kivy import Logger
from src.note_machine.cash_out.cashout_driver_base import CashoutDriver


class MockCashoutDriver(CashoutDriver):

    def start_cashout(self) -> None:
        pass

    def stop_cashout(self) -> None:
        pass

    def get_balance(self) -> dict[int, int]:
        balance = {}
        for note in CashoutDriver._MAP_CHANNEL_NOTES.values():
            Logger.debug("Adding 1 of note: {} to mock counter".format(note))
            balance[note] = 1
            time.sleep(0.1)
        return balance

    def do_cashout(self, amount: int, currency: str = "CHF") -> None:
        print("Cashout: {} {}".format(amount, currency))
