#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from threading import Lock
from typing import Any, Optional

import strictyaml
from kivy import Logger
from kivy.app import App
from kivy.clock import mainthread
from kivy.core.text import LabelBase
from kivy.properties import DictProperty, NumericProperty, StringProperty
from kivy.uix.screenmanager import RiseInTransition, ScreenManager
from path import Path

from src.app.components.label_sb import LabelSB
from src.app.components.overlay import OverlayNotSync
from src.app.screens.main import ScreenMain
from src.config import Config
from src.node.pricefeed_subscriber import Prices
from src.node.rpc import NodeRPC
from src.node.zmqpoller import ZMQPoller
from src.qr.scanner import get_scanner
from src.qr.scanner.qr_scanner_base import QrScanner


class TemplateApp(App):
    node_rpc: NodeRPC

    """Selected language as an kivy's Observable"""
    _selected_language = StringProperty('EN')
    _stablecoin_reserve = NumericProperty(-1e18)
    _eth_reserve = NumericProperty(-1e18)
    kv_directory = 'src/app/template'
    prices = DictProperty()

    def __init__(self, config: Config) -> None:
        super().__init__()

        LabelBase.register(name='SpaceGrotesk', fn_regular='assets/fonts/SpaceGrotesk-Regular.ttf')

        self._labels: list[LabelSB] = []
        self._config = config

        self._qr_scanner = get_scanner(self._config.camera)
        self.node_rpc = NodeRPC(self._config.zmq.rpc)

        self.zmq_poller = ZMQPoller(
            addr_status=config.zmq.status,
            addr_prices=f"{config.zmq.pricefeed.address}:{config.zmq.pricefeed.port_sub}",
            app=self,
            name="ZMQpoller",
            daemon=True,
        )

        # Physical fiat currency supported by the banknote validator
        self._fiat_currency = config.fiat_currency

        self._manager: Optional['Manager'] = None
        self._languages = self._retrieve_lang('src/app/resources/lang_template.yaml')
        self._popup_sync = OverlayNotSync()
        self._overlay_lock = Lock()

        self._popup_count = 0

    def build(self) -> Optional['Manager']:
        # Get a language
        self._selected_language = next(iter(self._languages))

        # Must be called after setting _selected_language
        self._manager = Manager(self._config, transition=RiseInTransition())
        self.zmq_poller.start()

        return self._manager

    def change_language(self, selected_language: str) -> None:
        """
        Change the current language.

        :param selected_language: Which language to select
        """
        self._selected_language = selected_language

        for label in self._labels:
            label.update_text()

    def add_translatable(self, label: LabelSB) -> None:
        self._labels.append(label)

    def get_config(self) -> Config:
        """Get app config"""
        return self._config

    def get_string(self, id: str, params: Optional[list[object]] = None) -> str:
        """
        Get a specific string by ID, using the current language.

        :param id:      id of the requested string
        :param params:  params for string formatting
        """
        try:
            t = self._languages[self._selected_language][id]
            if params:
                t = t.format(*params)
            return t
        except KeyError:
            Logger.error(
                f"swapbox: translations missing for: dict={self._selected_language};key={id}")
            return f"[undef, id={id}, l={self._selected_language}]"

    def format_fiat_price(self, value: int, decimals: int = 0) -> str:
        """
        Return a formatted price with given value and swapbox currency.
        :param value: The price as a human-readable value
        :param decimals: The amount of decimals you want to show
        :return: The formatted price
        """
        # TODO: handle prefixed AND suffixed price format ($ 10 and 10 EUR).
        currency = self._fiat_currency

        if currency == "EUR":
            currency = "€"
        if currency == "USD":
            currency = "$"

        return f"{currency} {value:.{decimals}f}"

    @staticmethod
    def format_crypto_price(value: int, token_name: str, decimals: int = 0) -> str:
        """
        Return a formatted price with given value and a curre
        :param value: The price as a human-readable value
        :param token_name: The token name
        :param decimals: The amount of decimals you want to show
        :return: The formatted price
        """
        return f"{value:.{decimals}f} {token_name}"

    @staticmethod
    def format_small_address(address: str) -> str:
        return f"{address[0:9]}...{address[-8:-1]}"

    @staticmethod
    def _retrieve_lang(path: str) -> dict[str, dict[str, str]]:
        """
        Load and parses language file...
        :param path: path of the language file
        :return: language dict
        """
        languages_yaml = strictyaml.load(Path(path).bytes().decode('utf8')).data
        return {k: dict(v) for k, v in languages_yaml.items()}

    @mainthread  # type: ignore[misc]
    def show_sync_popup(self, *_: Any) -> None:
        self._popup_sync.open()

    @mainthread  # type: ignore[misc]
    def hide_sync_popup(self, *_: Any) -> None:
        self._popup_sync.dismiss()

    @mainthread  # type: ignore[misc]
    def update_prices(self, prices: Prices, *_: Any) -> None:
        self.prices = prices

    def on_stop(self) -> None:
        self.node_rpc.stop()
        self.zmq_poller.stop()
        self.zmq_poller.join(5)

    def before_popup(self) -> None:
        with self._overlay_lock:
            self._popup_count += 1
            if self._popup_count == 1:
                self._qr_scanner.hide_overlay()

    def after_popup(self) -> None:
        with self._overlay_lock:
            self._popup_count -= 1
            if self._popup_count == 0:
                self._qr_scanner.show_overlay()

    def get_qr_scanner(self) -> QrScanner:
        return self._qr_scanner


class Manager(ScreenManager):
    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._main_screen = ScreenMain(config, name='main')
        self.add_widget(self._main_screen)

    def set_content_screen(self, screen_id: str) -> None:
        self._main_screen.set_current_screen(screen_id)
