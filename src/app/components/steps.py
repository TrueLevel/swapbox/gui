#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum
from typing import Any

from kivy.app import App
from kivy.lang import Builder

from src.app.components.boxlayout_bg import BoxLayoutBackground

Builder.load_string('''
<IconStep@Image>
    disabled: True
    color: color_off_white
    disabled_color: color_gray_3

<StepLabel@LabelLeft>
    disabled: True
    color: color_off_white
    disabled_color: color_gray_3

<StepsWidgetBuy>
    padding: (30, 30)
    background_color: color_darker_black
    BoxLayout:
        orientation: "vertical"
        spacing: 10

        LabelLeft:
            size_hint_y: 0.1
            text_id: 'your_selection'

        BoxLayout:
            orientation: "horizontal"

            BoxLayout:
                orientation: "vertical"
                size_hint_x: 0.4

                IconStep:
                    id: ico_action
                    source: "assets/img/action.png"
                IconStep:
                    id: ico_network
                    source: "assets/img/network.png"
                IconStep:
                    id: ico_currency
                    source: "assets/img/currency.png"
                IconStep:
                    id: ico_wallet
                    source: "assets/img/wallet.png"
                IconStep:
                    id: ico_amount
                    source: "assets/img/cash.png"

            BoxLayout:
                orientation: "vertical"

                StepLabel:
                    id: label_action
                    text_id: "step_action_deposit"
                StepLabel:
                    id: label_network
                    text_id: "step_network" if self.disabled else ""
                StepLabel:
                    id: label_currency
                    text_id: "step_currency" if self.disabled else ""
                StepLabel:
                    id: label_wallet
                    text_id: "step_wallet" if self.disabled else ""
                StepLabel:
                    id: label_amount
                    text_id: "step_amount" if self.disabled else ""

<StepsWidgetSell>
    padding: (30, 30)
    background_color: color_darker_black
    BoxLayout:
        orientation: "vertical"
        spacing: 10

        LabelLeft:
            size_hint_y: 0.1
            text_id: 'your_selection'

        BoxLayout:
            orientation: "horizontal"

            BoxLayout:
                orientation: "vertical"
                size_hint_x: 0.4

                IconStep:
                    id: ico_action
                    source: "assets/img/action.png"
                IconStep:
                    id: ico_amount
                    source: "assets/img/cash.png"
                IconStep:
                    id: ico_network
                    source: "assets/img/network.png"
                IconStep:
                    id: ico_currency
                    source: "assets/img/currency.png"
                IconStep:
                    id: ico_wallet
                    source: "assets/img/wallet.png"

            BoxLayout:
                orientation: "vertical"

                StepLabel:
                    id: label_action
                    text_id: "step_action_withdraw"
                StepLabel:
                    id: label_amount
                    text_id: "step_amount" if self.disabled else ""
                StepLabel:
                    id: label_network
                    text_id: "step_network" if self.disabled else ""
                StepLabel:
                    id: label_currency
                    text_id: "step_currency" if self.disabled else ""
                StepLabel:
                    id: label_wallet
                    text_id: "step_wallet" if self.disabled else ""
''')


class Wallet(Enum):
    PAPER = 0
    HOT = 1


class StepsWidgetBase(BoxLayoutBackground):

    def __init__(self, **kwargs: Any) -> None:
        super(StepsWidgetBase, self).__init__(**kwargs)
        self._app = App.get_running_app()

    def set_network(self, network: str) -> None:
        self.ids.ico_network.disabled = False
        self.ids.label_network.disabled = False
        self.ids.label_network.text = network

    def set_token(self, token_symbol: str) -> None:
        self.ids.ico_currency.disabled = False
        self.ids.label_currency.disabled = False
        self.ids.label_currency.text = token_symbol

    def set_amount_fiat(self, amount_fiat: str) -> None:
        self.ids.ico_amount.disabled = False
        self.ids.label_amount.disabled = False
        self.ids.label_amount.text = amount_fiat


class StepsWidgetBuy(StepsWidgetBase):
    pass


class StepsWidgetSell(StepsWidgetBase):
    pass
