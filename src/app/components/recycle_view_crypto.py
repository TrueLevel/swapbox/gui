from typing import Any, Optional, TypedDict

from kivy.properties import BooleanProperty, Logger
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleKVIDsDataViewBehavior

from src.config import Token


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior, RecycleBoxLayout):
    """Adds selection and focus behaviour to the view."""


class TokenListItem(RecycleKVIDsDataViewBehavior, BoxLayout):
    """Add selection support to the Label."""
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv: Any, index: int, data: dict[str, Any]) -> None:
        """Catch and handle the view changes."""
        self.index = index
        self.disabled = data['price.translate']  # Disable tokens without prices
        super(TokenListItem, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch: Any) -> bool:
        """Add selection on touch down."""
        if super(TokenListItem, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)
        return False

    def apply_selection(self, rv: Any, index: int, is_selected: bool) -> None:
        """Respond to the selection of items in the view."""
        self.selected = is_selected
        if is_selected:
            rv.set_selected(index)


TokenData = TypedDict('TokenData', {
    'token': Token,
    'symbol.text': str,
    'name.text': str,
    'value': str,
    'price.text': str,
    'price.text_id': str,
    'price.translate': bool
})


class TokensRecycleView(RecycleView):
    ICONS_FOLDER = "assets/img/currency_icons/{}.png"

    def __init__(self, **kwargs: Any) -> None:
        super(TokensRecycleView, self).__init__(**kwargs)
        self.selected = -1
        self.data: list[TokenData] = []

    def populate(self, tokens: list[Token]) -> None:
        self.data = [{
            'token': token,
            'symbol.text': token.symbol,
            'name.text': token.name,
            'value': self.ICONS_FOLDER.format(token.symbol.lower()),
            'price.text': '',
            'price.text_id': 'loading',
            'price.translate': True,
        } for token in tokens]

    def update_prices(self, prices: dict[str, str]) -> bool:
        """
        Update token prices inside list view.

        :param prices: Dictionnary of new prices where the key is the token
        symbol and value is the display price. It will try to match the token
        name (key) with existing rows, if it exists in the list, then the price
        is updated.

        :return True if a valid price was received, False otherwise.
        """
        valid_price = False

        for i, _ in enumerate(self.data):
            try:
                price = prices[self.data[i]['token'].address]
            except KeyError:
                self.data[i]['price.translate'] = True
            else:
                self.data[i]['price.translate'] = False
                self.data[i]['price.text'] = price
                valid_price = True

        if self.selected < 0 <= (candidate := next(
                (i for i, token in enumerate(self.data) if not token['price.translate']), -1)):
            self.ids.controller.selected_nodes = [candidate]

        self.refresh_from_data()
        return valid_price

    def set_selected(self, index: int) -> None:
        self.selected = index

    def get_selected_token(self) -> Optional[Token]:
        """Return selected token from the list view."""
        if self.selected < 0 or self.selected >= len(self.data):
            return None

        return self.data[self.selected]['token']

    def deselect(self) -> None:
        self.selected = -1
        self.layout_manager.deselect_node(self.selected)
