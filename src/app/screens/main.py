# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Any

from kivy.uix.screenmanager import Screen

from src.app.screens.base import SBScreen
from src.app.screens.buy import ScreenBuyFinal, ScreenBuyInsert, ScreenBuyScan, ScreenSelectCrypto
from src.app.screens.sell import ScreenSellAmount, ScreenSellScan, ScreenSellSelectToken
from src.app.screens.setup import ScreenSetup1, ScreenSetup2, ScreenSetup3
from src.app.screens.workflow import WorkflowMixin
from src.config import Config


class ScreenMain(SBScreen):
    screen_name = 'main'

    def __init__(self, config: Config, **kwargs: Any) -> None:
        self.config = config
        super().__init__(**kwargs)
        sm = self.ids.sm_content
        menu_screen = 'menu'
        sm.add_widget(ScreenMenu(name=menu_screen))
        sm.add_widget(ScreenSettings(name='settings'))

        self.add_workflow(menu_screen, (
            ScreenSelectCrypto(config),
            ScreenBuyScan(config),
            ScreenBuyInsert(config),
            ScreenBuyFinal()
        ))

        self.add_workflow(menu_screen, (
            ScreenSellAmount(config),
            ScreenSellSelectToken(config),
            ScreenSellScan(config)
        ))

        sm.add_widget(ScreenSetup1(name='setup_1'))
        sm.add_widget(ScreenSetup2(name='setup_2'))
        sm.add_widget(ScreenSetup3(name='setup_3'))

        self._sm = sm

    def set_current_screen(self, screen_id: str) -> None:
        self._sm.transition.direction = "down"
        self._sm.current = screen_id

    def show_help(self) -> None:
        # TODO
        print("show help")

    def back_to_menu(self) -> None:
        self._sm.transition.direction = "right"
        self._sm.current = 'menu'

    def add_workflow(self, back_screen: str, screens: tuple[WorkflowMixin, ...]) -> None:
        if len(screens) < 1:
            raise ValueError("Workflow needs at least 1 screens")

        sm = self.ids.sm_content
        prev_screen = back_screen
        for i, screen in enumerate(screens[:-1]):
            next_screen = screens[i + 1].screen_name

            sm.add_widget(screen)
            screen.set_workflow(prev_screen, next_screen)

            prev_screen = screen.screen_name

        next_screen = back_screen

        screen = screens[-1]
        sm.add_widget(screen)
        screen.set_workflow(prev_screen, next_screen)


class ScreenMenu(SBScreen):
    def buy_crypto(self) -> None:
        self.manager.transition.direction = "left"
        self.manager.current = ScreenSelectCrypto.screen_name

    def sell_crypto(self) -> None:
        self.manager.transition.direction = "left"
        self.manager.current = ScreenSellAmount.screen_name


class ScreenSettings(Screen):
    pass
