# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from functools import partial
from threading import Thread
from typing import Any, Optional

from kivy import Logger
from kivy.app import App
from kivy.clock import Clock, mainthread
from kivy.properties import NumericProperty, ObjectProperty, StringProperty
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.modalview import ModalView

from src.app.components.recycle_view_crypto import TokensRecycleView
from src.app.components.steps import StepsWidgetBuy
from src.app.screens.base import SBScreen
from src.app.screens.workflow import WorkflowMixin
from src.app.types.tx import TransactionReceipt
from src.config import Config, Token
from src.led import load_led_driver
from src.note_machine import cash_in
from src.qr.generator import QRGenerator
from src.qr.scanner.util import parse_address
from src.app.components.qrreader import QRReader
from pydantic import ValidationError


class BuyScreen(WorkflowMixin, SBScreen):

    def on_pre_enter(self, *args: Any) -> None:
        steps: StepsWidgetBuy = self.ids.steps
        if network := self.get_network():
            steps.set_network(network)

        if (token := self.get_token()) is not None:
            steps.set_token(token.symbol)

        # TODO Handle Wallet Types

        if (amount_fiat := self.get_fiat_amount()) > 0:
            formatted_amount = self._app.format_fiat_price(amount_fiat)
            steps.set_amount_fiat(formatted_amount)

    def get_token(self) -> Optional[Token]:
        return self.manager.get_screen(ScreenSelectCrypto.screen_name).get_selected_token()

    def get_network(self) -> str:
        return self.manager.get_screen(ScreenBuyScan.screen_name).network

    def get_to_addr(self) -> str:
        return self.manager.get_screen(ScreenBuyScan.screen_name).addr

    def get_fiat_amount(self) -> int:
        return self.manager.get_screen(ScreenBuyInsert.screen_name).fiat_amount

    def get_receipt(self) -> Optional[TransactionReceipt]:
        return self.manager.get_screen(ScreenBuyInsert.screen_name).receipt


class ScreenSelectCrypto(BuyScreen):
    """Screen for token selection."""

    screen_name = "buy_select"

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._app = App.get_running_app()

        # init recycle view
        self._list_view: TokensRecycleView = self.ids.rv_tokens
        self._list_view.populate(config.backends[0].tokens)

    def on_pre_enter(self, *args: Any) -> None:
        """
        KV Lifecycle: Before 'enter screen' animation.
        """
        super().on_pre_enter(*args)

        # select first node default
        self.ids.rv_tokens.deselect()
        self.ids.button_confirm.disabled = True

        self._app.bind(prices=self._update_prices)
        self._app.zmq_poller.update_prices.set()

    def on_enter(self, *args: Any) -> None:
        """
        Fix no event fired when price doesn't change.
        """
        super().on_enter(*args)
        self._update_prices()

    def on_leave(self, *_: list[Any]) -> None:
        self._app.zmq_poller.update_prices.clear()
        self._app.unbind(prices=self._update_prices)

    def _update_prices(self, *_: list[Any]) -> None:

        valid_price = self.ids.rv_tokens.update_prices({
            token: self._app.format_fiat_price(price.net_buy_price, 4)
            for token, price in self._app.prices.items()
        })

        if self.ids.button_confirm.disabled and valid_price:
            self.ids.button_confirm.disabled = False

    def get_selected_token(self) -> Optional[Token]:
        return self._list_view.get_selected_token()


class ScreenBuyScan(BuyScreen):
    """Screen for scanning user address."""

    screen_name = "buy_scan"

    addr = StringProperty(defaultvalue="")
    network = StringProperty(defaultvalue="")

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._app = App.get_running_app()
        self._qr_scanner = self._app.get_qr_scanner()

        self._led_driver = load_led_driver(config.relay_method)
        self._hide_scan_evt = Clock.create_trigger(self._hide_scan_err, timeout=10)

    def on_pre_enter(self, *args: Any) -> None:
        """
        KV Lifecycle: Before 'enter screen' animation.
        """
        super().on_pre_enter(*args)

        Thread(target=self._start_scan, daemon=True).start()

    def on_leave(self) -> None:
        """
        KV Lifecycle: After 'leave screen' animation.
        """
        self._qr_scanner.stop_scan()

    def parse_qr_address(self, qr: str) -> None:
        try:
            network, address = parse_address(qr)
        except ValueError as err:
            Logger.error(f"BUY: failed to parse address from QR '{qr}': {err}")
            Clock.schedule_once(partial(self._show_scan_err, str(err)))
        else:
            Clock.schedule_once(partial(self._set_tx_addr, network, address))

    def _start_scan(self) -> None:

        self._led_driver.led_on()
        if not self._qr_scanner.sync:
            qr = self._qr_scanner.scan(self.qr_reader, self.parse_qr_address)  
        else:
            qr = self._qr_scanner.scan()
            self._led_driver.led_off()
            self.parse_qr_address(qr)
            Logger.debug("QR: Stop scanning")

    @mainthread  # type: ignore[misc]
    def _show_scan_err(self, msg: str, *_: list[Any]) -> None:
        self.ids.scan_error.text = msg
        self.ids.scan_error.opacity = 1
        self._hide_scan_evt.cancel()
        self._hide_scan_evt()

    @mainthread  # type: ignore[misc]
    def _hide_scan_err(self, *_: list[Any]) -> None:
        self.ids.scan_error.opacity = 0

    @mainthread  # type: ignore[misc]
    def _set_tx_addr(self, network: Optional[str], addr: Optional[str], *_: list[Any]) -> None:
        if not network:
            Logger.debug("BUY: QR network not found")
            self.manager.transition.direction = "right"
            self.manager.current = "menu"

        if not addr:
            Logger.debug("BUY: QR addr not found")
            self.manager.transition.direction = "right"
            self.manager.current = "menu"

            return

        self.network = network
        self.addr = addr
        self.goto_next()


class ScreenBuyInsert(BuyScreen):
    screen_name = "buy_insert"

    _label_address_to = StringProperty()
    _label_minimum_received = StringProperty()
    _label_max_amount = StringProperty()
    _token_symbol = StringProperty()
    _token_decimals_display = NumericProperty(6)
    _token_price = NumericProperty(1)

    _estimated_crypto_amount = NumericProperty()
    _min_buy_amount = NumericProperty()
    _min_buy_amount_wei = NumericProperty()
    fiat_amount = NumericProperty(defaultvalue=0)
    token_decimals = NumericProperty(defaultvalue=0)
    token_addr = StringProperty()
    receipt = ObjectProperty(allownone=True)

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._app = App.get_running_app()

        self._valid_notes = config.note_machine.notes_values
        self._cash_in = cash_in.get_driver(config.note_machine, self._update_cashin)
        self._buy_limit = config.buy_limit
        self._label_max_amount = self._app.format_fiat_price(self._buy_limit)

        self._token_price = 1.0

    def on_pre_enter(self, *args: Any) -> None:
        """
        KV Lifecycle: Before 'enter screen' animation.
        """
        super().on_pre_enter(*args)

        self.fiat_amount = 0
        self.receipt = None
        self._label_address_to = self.get_to_addr()

        if (token := self.get_token()) is not None:
            self.token_decimals = token.decimals
            self.token_addr = token.address
            self._token_symbol = token.symbol
        else:
            Logger.info("BUY: token not received")

        self._cash_in.start_cashin()
        self._app.bind(prices=self._update_prices)
        self._app.zmq_poller.update_prices.set()

    def on_enter(self, *args: Any) -> None:
        """
        Fix no event fired when price doesn't change.
        """
        super().on_enter(*args)
        self._update_prices()

    def on_leave(self) -> None:
        self._cash_in.stop_cashin()
        self.ids.button_confirm.text_id = "buy"

        self._app.zmq_poller.update_prices.clear()
        self._app.unbind(prices=self._update_prices)

    def goto_next(self) -> None:
        if not self._buy():
            # TODO: handle failure better than this ?
            self.manager.transition.direction = "right"
            self.manager.current = self.screen_name  # LOL! WTF?
            return

        super().goto_next()

    def _update_prices(self, *_: list[Any]) -> None:

        if not self.token_addr:
            return

        token_prices = self._app.prices.get(self.token_addr)
        if token_prices is None:
            Logger.info(f"BUY: token price missing for {self.token_addr}")
            return

        self._token_price = token_prices.net_buy_price
        self._update_price_labels()

    def _update_cashin(self, message: str) -> None:
        """
        Callback when the cashin thread sends an update.
        :param message: The cashin thread raw message. Format: <currency>:<amount>
        """
        try:
            cash_in = int(message.split(":")[1])
        except ValueError as e:
            Logger.error("BUY: failed to parse cashin message: {msg}", e)
            # cash out whatever happened here ?
            return
        Logger.info("BUY: cashin message: %s" % cash_in)

        if cash_in in self._valid_notes:
            self.fiat_amount += cash_in
            self._update_price_labels()

            # Disable Stop and Back button
            self.parent.parent.parent.ids.stop_button.disabled = True
            self.ids.button_back.disabled = True

            # for this limit to be half effective we must only accept notes smaller than the limit
            if self.fiat_amount >= self._buy_limit:
                self._cash_in.stop_cashin()
                self._highlight_max_amount()

    def _highlight_max_amount(self) -> None:
        """Highlights the max_amount label (title and value)"""
        self.ids.max_amount_title.color = "red"
        self.ids.max_amount_text.color = "red"

    def _update_price_labels(self) -> None:
        if self.fiat_amount > 1:
            Clock.schedule_once(self._price_quote)

    def _price_quote(self, dt: Any) -> None:
        """Send a pricequote request to the proxy"""
        try:
            response = self._app.node_rpc.price_quote(
                self.fiat_amount,
                self.token_addr
            )

            self._estimated_crypto_amount = response.buy_amount
            self._min_buy_amount = response.min_buy_amount
            self._min_buy_amount_wei = int(self._min_buy_amount * 10 ** self.token_decimals)


            Logger.debug(f"PRICEQUOTE: rpc_resp={response}")
        except ValidationError as e:
            Logger.error(f"NODE_RPC ERROR: {e}") # TO-DO: show nice error message

    def _buy(self) -> bool:
        """Sends a buy order to the node RPC, in a thread"""
        # disable confirm button
        button_confirm: Button = self.ids.button_confirm
        button_confirm.disabled = True
        button_confirm.text_id = "please_wait"

        self._cash_in.stop_cashin()

        try:
            response = self._app.node_rpc.buy(
                self.fiat_amount,
                self.token_addr,
                self._min_buy_amount_wei,
                self._label_address_to,
            )

            Logger.debug(f"BUY: rpc_resp={response}")

            if (success := response.status == "success") and response.receipt is not None:
                self.receipt = response.receipt

                # Reenable Stop and Back button
                self.parent.parent.parent.ids.stop_button.disabled = False
                self.ids.button_back.disabled = False

            return success
        except ValidationError as e:
            Logger.error(f"NODE_RPC ERROR: {e}") # TO-DO: show nice error message

        return False


class ScreenBuyFinal(BuyScreen):
    screen_name = "buy_final"

    _fiat_amount = NumericProperty(0.0)
    _crypto_bought = NumericProperty(0.0)
    _token_symbol = StringProperty("")

    _fees_total = NumericProperty(0.0)
    _fee_percent = NumericProperty(0.0)

    _label_address_to = StringProperty("")

    _qr_uri = StringProperty("")

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._app = App.get_running_app()
        self.details_popup = FeesDetailsModal(size_hint=(0.5, 0.4))

        self.img_qr: Image = self.ids.img_qr

    def on_pre_enter(self, *args: Any) -> None:
        super().on_pre_enter(*args)

        self._fiat_amount = self.get_fiat_amount()
        if (receipt := self.get_receipt()) is None:
            Logger.error("BUY: missing receipt")
            return

        self._crypto_bought = receipt.amount_bought / 10 ** receipt.token_decimals
        self._fee_percent = ((receipt.fees.operator / self._fiat_amount) * 100.0) + (receipt.fees.liquidity_provider * 100.0)
        self._fees_total = receipt.fees.operator + (receipt.fees.liquidity_provider * self._fiat_amount)


        if (token := self.get_token()) is None:
            Logger.error("Buy: missing token")
        else:
            self._token_symbol = token.symbol

        self._label_address_to = self.get_to_addr()
        self._qr_uri = QRGenerator.generate_qr_image(receipt.url)

        self.details_popup.set_data(self._fiat_amount, receipt, self._token_symbol)

    def on_enter(self, *args: Any) -> None:
        self.img_qr.source = self._qr_uri

    def show_details(self) -> None:
        self.details_popup.open()


class FeesDetailsModal(ModalView):
    fee_operator = NumericProperty()
    fee_operator_percent = NumericProperty()
    # fee_network = NumericProperty()
    # fee_network_fiat = NumericProperty()
    # fee_network_percent = NumericProperty()
    fee_lp = NumericProperty()
    fee_lp_percent = NumericProperty()
    fee_total = NumericProperty()
    fee_total_percent = NumericProperty()
    token_symbol = StringProperty()

    def set_data(self, fiat_amount: int, tx: TransactionReceipt, token_symbol: str) -> None:
        self.fee_operator = tx.fees.operator
        self.fee_operator_percent = (tx.fees.operator / fiat_amount) * 100.0
        # self.fee_network = tx.fees.network / 10 ** 18
        # self.fee_network_fiat = tx.fees.network_in_fiat
        # self.fee_network_percent = tx.fees.network_in_fiat / fiat_amount * 100.0
        self.fee_lp = tx.fees.liquidity_provider * fiat_amount
        self.fee_lp_percent = tx.fees.liquidity_provider * 100.0
        self.fee_total = self.fee_lp + self.fee_operator
        self.fee_total_percent = self.fee_operator_percent + self.fee_lp_percent
        self.token_symbol = token_symbol
