# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Any

from kivy.uix.screenmanager import Screen


class SBScreen(Screen):
    """Base Swapbox screen.

    Swapbox screens are intended to be singletons.
    The screen name, defined in Kivy with `.name` is set to be the class
    attribute `screen_name`. This allows the reference of a screen instance from
    the class. By default, the `screen_name is the class `__name__`.
    """
    screen_name = ''

    def __new__(cls, *args: Any, **kwargs: Any) -> 'SBScreen':
        inst = super().__new__(cls, *args, **kwargs)
        inst.screen_name = cls.screen_name or cls.__name__
        return inst

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        if not self.name:  # type: ignore[has-type]
            self.name = self.screen_name
