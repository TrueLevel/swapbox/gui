#  Swap-box
#  Copyright (c) 2022 TrueLevel SA
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Any, Callable, Optional

from kivy import Logger
from kivy.app import App
from kivy.properties import AliasProperty, NumericProperty, ObjectProperty, StringProperty

from src.app.components.buttons import ButtonDark, ButtonLight
from src.app.components.recycle_view_crypto import TokensRecycleView
from src.app.components.steps import StepsWidgetSell
from src.app.screens.base import SBScreen
from src.app.screens.workflow import WorkflowMixin
from src.config import Config, Token
from src.node.pricefeed_subscriber import Price
from src.note_machine import cash_out
from src.qr.generator import QRGenerator


class NoteButton(ButtonLight):
    def __init__(self, value: int, callback: Callable[[int], None], **kwargs: Any) -> None:
        super(NoteButton, self).__init__(**kwargs)
        self.value = value
        self.callback = callback
        self.text = f"+ {self.value}"

    def on_press(self) -> None:
        self.callback(self.value)


class SellScreen(WorkflowMixin, SBScreen):
    """Base class for screens of the sell workflow."""

    def get_steps(self) -> StepsWidgetSell:
        return self.ids.steps

    steps = AliasProperty(get_steps)

    def on_pre_enter(self, *args: Any) -> None:
        # self.steps.set_action(Action.SELL)
        self.steps.set_network("zkSync")

        if (token := self.get_token()) is not None:
            self.steps.set_token(token.symbol)

        # TODO Handle Wallet Types

        if (amount_fiat := self.get_fiat_amount()) > 0:
            formatted_amount = self._app.format_fiat_price(amount_fiat)
            self.steps.set_amount_fiat(formatted_amount)

    def set_fiat_amount(self, amount_fiat: int) -> None:
        steps: StepsWidgetSell = self.ids.steps
        formatted_amount = self._app.format_fiat_price(amount_fiat)
        steps.set_amount_fiat(formatted_amount)

    def get_token(self) -> Optional[Token]:
        return self.manager.get_screen(ScreenSellSelectToken.screen_name).get_selected_token()

    def get_fiat_amount(self) -> int:
        return self.manager.get_screen(ScreenSellAmount.screen_name).fiat_amount

    def get_crypto_amount(self) -> float:
        return self.manager.get_screen(ScreenSellSelectToken.screen_name).amount_crypto


class ScreenSellAmount(SellScreen):
    """Screen in the sell process to choose the amount to withdraw."""
    screen_name = "sell_amount"

    fiat_amount = NumericProperty(0)
    _label_max_amount = StringProperty("")

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._app = App.get_running_app()

        self._cash_out = cash_out.get_driver(config.note_machine)
        self._valid_notes = config.note_machine.notes_values
        self._currency = config.fiat_currency
        self._buy_limit = config.buy_limit
        self._label_max_amount = self._app.format_fiat_price(self._buy_limit)

        self._note_balance: dict[int, int] = {}

    def on_pre_enter(self, *args: Any) -> None:
        super().on_pre_enter(*args)

        self.fiat_amount = 0

        # TODO Setup note btns async
        self.ids.grid_notes.clear_widgets()
        self.ids.grid_notes.add_widget(
            ButtonDark(text_id="loading", disabled=True))  # type: ignore[arg-type]

        self.ids.grid_notes.clear_widgets()
        self._note_balance = self._retrieve_note_balance()

        for note in self._valid_notes:
            note_button = NoteButton(note, self._add_amount, disabled=note > self._buy_limit)
            self.ids.grid_notes.add_widget(note_button)

        self.ids.grid_notes.add_widget(
            ButtonDark(text="clear", on_release=self._clear_amount))  # type: ignore[arg-type]
        self._cash_out.start_cashout()

    def _retrieve_note_balance(self) -> dict[int, int]:
        return self._cash_out.get_balance()

    def _clear_amount(self, *_: Any) -> None:  # Empty *args to match Kivy's API.
        self.fiat_amount = 0

    def _add_amount(self, amount: int) -> None:
        """Adds amount chose by the user to internal count."""
        if self._cash_out.check_available_notes(self._note_balance, self.fiat_amount +amount):
            if self.fiat_amount + amount <= self._buy_limit:
                self.fiat_amount += amount
            else:
                self.ids.max_amount_title.color = "red"
                self.ids.max_amount_text.color = "red"
        else:
            # TODO: tell user cash machine doesn't have a bill
            # Thread(target=self._threaded_buy, daemon=True).start()
            print("NotImplemented: Note not available")

        self.set_fiat_amount(self.fiat_amount)


class ScreenSellSelectToken(SellScreen):
    screen_name = "sell_select_token"

    amount_crypto = NumericProperty(0)

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._app = App.get_running_app()

        # self._tx_order: Optional[TransactionOrder] = None

        # init recycle view
        self._list_view: TokensRecycleView = self.ids.rv_tokens
        self._list_view.populate(config.backends[0].tokens)

    def on_pre_enter(self, *args: Any) -> None:
        super().on_pre_enter(*args)

        self.amount_crypto = 0

        # select first node default
        self.ids.rv_tokens.ids.controller.selected_nodes = [0]

        self._app.bind(prices=self._update_prices)
        self._app.zmq_poller.update_prices.set()

    def on_leave(self, *args: Any) -> None:
        self._app.zmq_poller.update_prices.clear()
        self._app.unbind(prices=self._update_prices)

    def goto_next(self) -> None:
        if (token := self.get_selected_token()) is not None:
            token_prices: Price = self._app.prices.get(token.address)
            price = token_prices.sell_price
            self.amount_crypto = self.get_fiat_amount() / price

        super().goto_next()

    def get_selected_token(self) -> Optional[Token]:
        return self._list_view.get_selected_token()

    def _update_prices(self, *_: Any) -> None:
        self.ids.rv_tokens.update_prices({
            token: self._app.format_fiat_price(price.sell_price, 4)
            for token, price in self._app.prices.items()
        })


class ScreenSellScan(SellScreen):
    screen_name = "sell_scan"

    _payment_address_ether = StringProperty("")
    _qr_image_uri = ObjectProperty()

    _sell_amount = NumericProperty(0.)

    _token = StringProperty()

    def __init__(self, config: Config, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._app = App.get_running_app()

        self._valid_notes = config.note_machine.notes_values
        self._backends = config.backends

    def on_pre_enter(self, *args: Any) -> None:
        super().on_pre_enter(*args)

        self._sell_amount = self.get_crypto_amount()
        self._payment_address_ether = self._get_backend_address()
        # TODO: generate qr code for any target currency
        img_uri = QRGenerator.generate_qr_image(self._payment_address_ether)
        self._qr_image_uri = img_uri

    def _get_backend_address(self) -> str:
        for backend in self._backends:
            if backend.type == "zkSync":
                return backend.address
        Logger.error(f"swapbox: no backend address for: zkSync")
        return ""
