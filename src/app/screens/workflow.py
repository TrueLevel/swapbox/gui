# Swap-box
# Copyright (C) 2019  TrueLevel SA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Any

from src.app.screens.main import SBScreen


class WorkflowMixin(SBScreen):
    """
    WorkflowMixin provides the logic to move between screens.

    A "workflow screen" movea back and forth to the previous or next screen.
    """

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.prev_screen, self.next_screen = '', ''

    def goto_next(self) -> None:
        self.manager.transition.direction = "left"
        self.manager.current = self.next_screen

    def goto_prev(self) -> None:
        self.manager.transition.direction = "right"
        self.manager.current = self.prev_screen

    def set_workflow(self, prev_screen: str, next_screen: str) -> None:
        self.prev_screen, self.next_screen = prev_screen, next_screen
